/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.ox_program;

import java.util.Scanner;

/**
 *
 * @author Nippon
 */
public class OX_Final {

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] box = new char[3][3];
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < box.length; i++) {
            for (int j = 0; j < box.length; j++) {
                box[i][j] = '-';
                System.out.print(box[i][j] + " ");
            }
            System.out.println("");
        }

        int count_turn = 0;
        do {
            char turn = 'O';
            if (count_turn % 2 == 0) {
                System.out.println("Turn O");
                System.out.println("Please input row,col:");
                turn = 'O';
            } else {
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                turn = 'X';
            }
            int row = kb.nextInt();
            int col = kb.nextInt();
            row = row - 1;
            col = col - 1;
            box[row][col] = turn; //รับค่าว่าใน turn นั้นเป็น XหรือO ที่จะเอามาเติม
            //Print Array 
            for (int k = 0; k < box.length; k++) {
                for (int j = 0; j < box.length; j++) {
                    System.out.print(box[k][j] + " ");
                }
                System.out.println("");
            }
            count_turn++;
            //Check Win!! 

            //แนวนอน
            if (box[0][0] == box[0][1] && box[0][2] == box[0][0] && box[0][0] != '-') {
                System.out.println(">>> " + box[0][0] + " win1 <<<");
                break;
            } else if (box[1][0] == box[1][1] && box[1][2] == box[1][0] && box[1][1] != '-') {
                System.out.println(">>> " + box[1][0] + " win2 <<<");
                break;
            } else if (box[2][0] == box[2][1] && box[2][2] == box[2][0] && box[2][2] != '-') {
                System.out.println(">>> " + box[2][0] + " win3 <<<");
                break;
            } //แนวตั้ง
            else if (box[0][0] == box[1][0] && box[2][0] == box[0][0] && box[0][0] != '-') {
                System.out.println(">>> " + box[0][0] + " win4 <<<");
                break;
            } else if (box[0][1] == box[1][1] && box[2][1] == box[0][1] && box[1][1] != '-') {
                System.out.println(">>> " + box[0][1] + " win5 <<<");
                break;
            } else if (box[0][2] == box[1][2] && box[2][2] == box[0][2] && box[2][2] != '-') {
                System.out.println(">>> " + box[0][2] + " win6 <<<");
                break;
            } //แนวทแยง
            else if (box[1][1] == box[0][0] && box[1][1] == box[2][2] && box[1][1] != '-') {
                System.out.println(">>> " + box[2][2] + " win7 <<<");
                break;
            } else if (box[1][1] == box[2][0] && box[1][1] == box[0][2] && box[1][1] != '-') {
                System.out.println(">>> " + box[2][2] + " win8 <<<");
                break;
            }
        } while (count_turn < 9);

       // System.out.println(">>> Draw <<<");

        
    }
}
